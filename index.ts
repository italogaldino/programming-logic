type Customer = {
    id: number,
    name: string,
    cpf: string,
    phone: string,
    address: string
};

type FnCallback = () => void;

type MenuOpt = {
    [opt: string]: {
        text: string,
        fnCallback: FnCallback | (() => void),
    }
};

let appON : boolean = true;

//Database
const dbCustomer : Customer[] = [];

//Menu Section
const createMenu = (): MenuOpt => {
    try {
        const menuOpt: MenuOpt = {
            "1": {text: "Cadastrar Cliente", fnCallback: insertCustomer},
            "2": {text: "Listar Clientes", fnCallback: listCustomers},
            "3": {text: 'Detalhar Cliente', fnCallback: getCustomer},
            "4": {text: 'Atualizar Cliente', fnCallback: updateCustomer},
            "5": {text: 'Deletar Cliente', fnCallback: deleteCustomer},
            "6": {text: 'Sair', fnCallback: () => appON = false}
        }
        return menuOpt
    } catch (e) {
        throw new Error(`Erro no menu: ${e}`)
    }
};

const checkOpt = (opt: any, menu: MenuOpt): number => {
    
    let optNumber = parseInt(opt);
    if (Number.isNaN(optNumber) || !menu[optNumber]) return -1;
    return optNumber
};

const getOpt = (menu: MenuOpt): number => {
    try {
        let menuString = "Selecione a ação desejada:\n";
        for (const i in menu) {
            const opt = menu[i];
            menuString += `${i}. ${opt.text}\n`;
        }

        let optSelected = prompt(menuString);
        let checkedOpt = checkOpt(optSelected, menu);
        
        while (checkedOpt == -1) {
            alert("Selecione uma opção válida");
            optSelected = prompt(menuString);
            checkedOpt = checkOpt(optSelected, menu);
        } 

        return checkedOpt;

    } catch (error) {
        throw error;
    }

};


//Features
const insertCustomer = () => {
    
    const name = prompt("Digite o nome do cliente:");
    const cpf = prompt("Digite o CPF do cliente:");
    const phone = prompt("Digite o telefone do cliente:");
    const address = prompt("Digite o endereço do cliente:");

    if (!name || !cpf || !phone || !address) return;

    const maxId = dbCustomer.length > 0 ? Math.max(...dbCustomer.map(el => el.id)) : 0;
    const id = maxId + 1;

    const customer: Customer = {id, name, cpf, phone, address};
    dbCustomer.push(customer);

    alert("Cliente cadastrado com sucesso");

};

const listCustomers = () => {
    if (dbCustomer.length === 0) {
        alert("Não existem clientes cadastrados");
    } else {
        const title = `Clientes cadastrados (${dbCustomer.length}):\n`;
        const items = dbCustomer.map(el => `ID: ${el.id} | Nome: ${el.name} |  CPF: ${el.cpf}\n`);
        alert(title + items.join(""));
    }
};

const getCustomer = () => {

    let idFind = prompt("Digite o ID do cliente a ser detalhado:");
    const idParsed = idFind ? parseInt(idFind) : null;
    const findedCustomer = dbCustomer.find(el => el.id == idParsed);

    if (!idParsed || !findedCustomer) {
        alert(`Cliente com ID: ${idFind} não foi encontrado`);
        return;
    }

    alert(`ID: ${findedCustomer.id}\nNome: ${findedCustomer.name}\nCPF: ${findedCustomer.cpf}\nTelefone: ${findedCustomer.phone}\nEndereço: ${findedCustomer.address}`);
};

const updateCustomer = () => {

    let idFind = prompt("Digite o ID do cliente que deseja atualizar:");
    const idParsed = idFind ? parseInt(idFind) : null;
    const findedCustomer = dbCustomer.find(el => el.id == idParsed);

    if (!idParsed || !findedCustomer) {
        alert(`Cliente com ID: ${idFind} não foi encontrado`);
        return;
    }

    alert(`O cadastro do cliente Nome: ${findedCustomer.name} | CPF: ${findedCustomer.cpf} será atualizado`);

    const name = prompt("Digite o nome do cliente:");
    const cpf = prompt("Digite o CPF do cliente:");
    const phone = prompt("Digite o telefone do cliente:");
    const address = prompt("Digite o endereço do cliente:");

    if (!name || !cpf || !phone || !address) {
        alert("Cadastro não foi atualizado");
        return;
    }

    findedCustomer.name = name;
    findedCustomer.cpf = cpf;
    findedCustomer.phone = phone;
    findedCustomer.address = address;

    alert("Cadastro atualizado com sucesso");

};

const deleteCustomer = () => {

    let idFind = prompt("Digite o ID do cliente a ser excluído:");
    const idParsed = idFind ? parseInt(idFind) : null;
    const findedCustomer = dbCustomer.find(el => el.id == idParsed);

    if (!idParsed || !findedCustomer) {
        alert(`Cliente com ID: ${idFind} não foi encontrado`);
        return;
    }

    dbCustomer.splice(dbCustomer.indexOf(findedCustomer), 1);

    alert("Cliente excluído da base")
};


//App Section
const execute = (): void => {
    try {

        const menu = createMenu();

        do {
            
            const menuSelected = getOpt(menu);
            menu[menuSelected].fnCallback();

        } while (appON);

    } catch (error) {
        alert(error);
    } finally {
        alert("Sistema encerrado!");
    }
}

//App Run
execute();
